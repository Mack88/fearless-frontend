import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <Router>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="/locations/new" element={<LocationForm/>} />
        <Route path="/conferences/new" element={<ConferenceForm/>} />
        <Route path="/presentations/new" element={<PresentationForm/>} />
        <Route path="/attendees" element={<AttendeesList />} />
        <Route path="/attendees/new" element={<AttendConferenceForm/>} />
      </Routes>
    </Router>
  );
}

export default App;
